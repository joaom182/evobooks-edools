var gulp = require('gulp');
var less = require('gulp-less');
var path = require('path');

var styles = {
    less: ['public/assets/less/*.less', 'public/assets/less/**/*.less', 'public/assets/less/**/**/*.less']
};

gulp.task('less', function () {
    return gulp.src(styles.less)
        .pipe(less({
            paths: [
                path.join(__dirname, 'public', 'assets', 'less', 'includes'),
                path.join(__dirname, 'public', 'assets', 'less', 'structure'),
                path.join(__dirname, 'public', 'assets', 'less', 'theme')
            ]
        }))
        .pipe(gulp.dest(path.join(__dirname, 'public', 'assets', 'css')));
});

gulp.task('watch', function () {
    gulp.watch(styles.less, ['less']);
});

gulp.task('default', ['watch', 'less']);